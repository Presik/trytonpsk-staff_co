# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import random
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.model.exceptions import AccessError
from trytond.i18n import gettext

conversor = None
try:
    from numword import numword_es
    conversor = numword_es.NumWordES()
except:
    print("Warning: Does not possible import numword module!")
    print("Please install it...!")

# Size photo default
WIDTH = 200
HEIGHT = 250


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'
    id_pass = fields.Char('Id. Pass', select=True, states={
            'readonly': True
        })
    salary_day = fields.Function(fields.Numeric('Salary Day',
            digits=(16, 2)), 'on_change_with_salary_day')
    photo = fields.Binary('Photo')
    photo_link = fields.Char('Photo Link')
    department = fields.Many2One('company.department', 'Department',
            required=False)
    boss = fields.Many2One('company.employee', 'Boss')
    salary_to_words = fields.Function(fields.Char('Amount to Pay (Words)',
            depends=['salary']), 'get_salary_to_words')

    @classmethod
    def __setup__(cls):
        super(Employee, cls).__setup__()

    @fields.depends('code', 'id_pass')
    def on_change_code(self, name=None):
        if self.code and self.code.isdigit():
            self.id_pass = self.get_ean8()

    @fields.depends('salary')
    def on_change_with_salary_day(self, name=None):
        if self.salary:
            return (self.salary / 30)
        else:
            return 0

    @classmethod
    def _get_password(cls):
        list_passwords = [employee.id_pass for employee in cls.search([])]
        invalid_id_pass = True
        while invalid_id_pass:
            id_pass = str(random.randrange(1001, 9999))
            if id_pass not in list_passwords:
                invalid_id_pass = False
        return id_pass

    def get_ean8(self):
        factor = 3
        x = 0
        for n in self.code:
            y = int(n) * factor
            x += y
            if factor == 3:
                factor = 1
            else:
                factor = 3
        res = (x % 10)
        if res ==  0:
            value = 0
        else:
            value = 10 - (x % 10)
        return self.code + str(value)

    def get_salary_to_words(self, name=None):
        if self.salary:
            return (conversor.cardinal(int(self.salary))).upper()
        else:
            return ''

    def get_salary(self, name=None):
        Date = Pool().get('ir.date')
        if self.contract:
            return self.contract.get_salary_in_date(Date.today())


class BankLetterReport(Report):
    "Bank Letter Report"
    __name__ = 'staff_co.bank_letter_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Company = Pool().get('company.company')
        report_context['company'] = Company(Transaction().context['company'])
        for rec in records:
            if not rec.party.bank_accounts:
                raise AccessError(gettext('msg_employee_not_bank', employee=rec.party.id_number))
        return report_context
