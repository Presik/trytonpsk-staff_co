# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import timedelta, date
from decimal import Decimal
from sql import Null
from sql.conditionals import Case
from dateutil.relativedelta import relativedelta

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.wizard import (
    Wizard, StateTransition, StateView, Button, StateReport)
from trytond.transaction import Transaction
from trytond.report import Report
from .exceptions import (StaffContractError)
from trytond.i18n import gettext

conversor = None
try:
    from numword import numword_es
    conversor = numword_es.NumWordES()
except:
    print("Warning: Does not possible import numword module!")
    print("Please install it...!")


class StaffContract(metaclass=PoolMeta):
    __name__ = 'staff.contract'
    transport_bonus = fields.Numeric('Transport Bonus', digits=(16, 2),
        states={'readonly': (Eval('state') != 'draft')})
    futhermores = fields.One2Many('staff.contract.futhermore', 'contract',
        'Futhermores', states={
          'readonly': Eval('state') == 'finished',
          })
    finished_date = fields.Date('Finished Date',
        help='Only set date when employee leaves the company')

    time_for_finished = fields.Function(fields.Integer('Time For Finish'),
        'get_time_for_finished', searcher='search_time_for_finished')
    salary_words = fields.Function(fields.Char('Amount to Pay (Words)',
        depends=['salary']), 'get_salary_to_words')
    transport_bonus_words = fields.Function(fields.Char('Amount to Pay (Words)',
        depends=['salary']), 'get_salary_to_words')
    require_trial_time = fields.Boolean('Require Trial Time', states={
        'readonly': Eval('state') == 'finished',
        })
    trial_time = fields.Function(fields.Date('Trial Time',
        depends=['require_trial_time'], states={
            'invisible': ~Eval('require_trial_time'),
        }), 'get_trial_time')
    duration_days = fields.Function(fields.Integer('Duration (Days)',
        help="In days", depends=['start_date', 'end_date']), 'get_duration')
    duration_months = fields.Function(fields.Integer('Duration (Months)',
        help="In months", depends=['start_date', 'end_date']), 'get_duration')
    futhermore_contract_end_date = fields.Function(fields.Date(
        'Futhermore Contract End Date'), 'get_futhermore_end_date')
    last_salary = fields.Function(fields.Numeric(
        'Last Salary', digits=(16, 2)), 'get_last_salary')
    cause_termination = fields.Char('Cause Termination', select=True)
    customer = fields.Many2One('party.party', 'Customer', states={
        'readonly': Eval('state') == 'finished',
    })
    week_work_days = fields.Integer('Week Work Days', select=True)

    @classmethod
    def __setup__(cls):
        super(StaffContract, cls).__setup__()
        cls.state_string = cls.state.translated('state')
    # @classmethod
    # def __register__(cls, module_name):
    #     super(StaffContract, cls).__register__(module_name)
    #     contracts = cls.search([
    #         ('finished_date', '!=', None),
    #     ])
    #     if len(contracts)<1:
    #         contracts = cls.search([])
    #         for contract in contracts:
    #             contract.finished_date = contract.get_finished_date()
    #             contract.save()

    @classmethod
    def search_time_for_finished(cls, name, clause):
        if type(clause[2]) != int:
            return
        delta = date.today() + timedelta(clause[2])
        contracts = cls.search([
            ('end_date', '!=', None),
        ])
        ids_ = []

        for c in contracts:
            finished_date = c.finished_date
            operator_ = clause[1]
            if operator_ == '=':
                operator_ = '=='
            try:
                if eval(f'{finished_date} {operator_} {delta}'):
                    ids_.append(c.id)
            except:
                pass
        return [('id', 'in', ids_)]

    @fields.depends('employee', 'salary')
    def on_change_employee(self):
        if self.employee and self.employee.salary:
            self.salary = self.employee.salary

    @fields.depends('contract_date', 'start_date')
    def on_change_contract_date(self):
        if self.contract_date:
            self.start_date = self.contract_date

    def _check_finish_date(self):
        super(StaffContract, self)._check_finish_date()
        today = Pool().get('ir.date').today()
        if self.finished_date and self.finished_date > today:
            raise StaffContractError(
                gettext('staff_co.msg_finish_contract_out_date'))

    @fields.depends('futhermores', 'end_date', 'state')
    def on_change_with_finished_date(self):
        finished_date = self.get_finished_date()
        if finished_date:
            self.finished_date = finished_date
        return finished_date

    @classmethod
    def canceled(cls, records):
        super(StaffContract, cls).canceled(records)
        for record in records:
            cls.write([record], {'finished_date': record.start_date})

    def get_finished_date(self, name=None):
        res = []
        if self.end_date:
            res.append(self.end_date)
        fu_dates = []
        for fu in self.futhermores:
            if fu.end_date and fu.kind in ('resignation', 'dismissal') and fu.state in ('confirmed', 'finished'):
                fu_dates.append(fu.end_date)
            elif fu.end_date and fu.kind in ('renewal', 'end_contract') and fu.state in ('confirmed', 'finished'):
                res.append(fu.end_date)
        if fu_dates:
            return min(fu_dates)
        elif res:
            return max(res)
        else:
            return None

    @classmethod
    def search_finished_date(cls, name, domain):
        ContractF = Pool().get('staff.contract.futhermore')
        contract = cls.__table__()
        contractf = ContractF.__table__()
        _, operator_, operand = domain

        if operand:
            operand = "'" + operand.strftime("%Y-%m-%d") + "'"
        else:
            operand = Null
        Operator = fields.SQL_OPERATORS[operator_]
        column_kind1 = Case((contractf.kind.in_(
            ['resignation', 'dismissal']), 1), else_=2).as_('kind1')

        table_tmp = (contractf.select(
            contractf.contract,
            contractf.end_date,
            column_kind1,
            distinct=True,
            distinct_on=[contractf.contract],
            where=(contractf.end_date != Null) & (contractf.state.in_(
                ['confirmed', 'finished'])),
            order_by=(
                contractf.contract, column_kind1, contractf.end_date.desc),
            ))

        union = (contract.join(
                table_tmp,
                type_='LEFT',
                condition=contract.id == table_tmp.contract
                ).select(contract.id,
                    contract.end_date,
                    table_tmp.end_date.as_('end_date_f'),
                    Case((table_tmp.end_date != Null, table_tmp.end_date), else_=contract.end_date).as_('finished_date'))
                    )
        query = (union.select(union.id,
                        where=Operator(union.finished_date, operand)))
        return [('id', 'in', query)]

    def get_trial_time(self, name):
        res = None
        if self.end_date and self.start_date and self.require_trial_time:
            delta = float((self.end_date - self.start_date).days) * 0.2
            res = self.start_date + timedelta(int(delta))
        return res

    def get_duration(self, name=None):
        res = None
        field_name = name[9:]
        if self.end_date and self.start_date:
            res = self.get_time_days(self.start_date, self.finished_date)
            if field_name != 'days':
                res = int(round(res / 30.0, 0))
        return res

    def get_salary_to_words(self, name=None):
        if getattr(self, name[:-6]):
            return (conversor.cardinal(int(getattr(self, name[:-6])))).upper()
        else:
            return ''

    def get_futhermore_end_date(self, name=None):
        res = []
        for f in self.futhermores:
            if f.kind != 'salary' and f.end_date and f.state in ('confirmed', 'finished'):
                res.append(f.end_date)
        if res:
            return max(res)

    def get_last_salary(self, name=None):
        # FIXME take last salary by date
        res = [self.salary]
        for f in self.futhermores:
            if f.kind == 'salary' and f.salary:
                res.append(f.salary)
        if res:
            return max(res)

    def get_salary_in_date(self, _date):
        res = []
        salary_dates = {}
        for f in self.futhermores:
            if f.kind == 'salary' and f.salary and f.futhermore_date <= _date:
                res.append(f.futhermore_date)
                salary_dates[f.futhermore_date] = f.salary
        if res:
            return salary_dates.get(max(res))
        else:
            return self.salary or 0

    def get_time_for_finished(self, name=None):
        res = []
        today = Pool().get('ir.date').today()
        if self.end_date:
            res.append(self.end_date)
        for f in self.futhermores:
            if f.kind != 'salary' and f.end_date and f.state in ('confirmed', 'finished'):
                res.append(f.end_date)
        max_date = max(res) if res else None
        if max_date and max_date > today:
            return (max(res) - today).days

    def count_leap_years(self, d):
        years = d.year
        if (d.month <= 2):
            years -= 1
        return int(years / 4) - int(years / 100) + int(years / 400)

    def get_time_days(self, start_date=None, end_date=None):
        if start_date and end_date:
            n = 1
            n1 = start_date.year * 360 + start_date.day
            for i in range(0, start_date.month - 1):
                n1 += 30
            n1 += self.count_leap_years(start_date)
            n2 = end_date.year * 360 + end_date.day
            for i in range(0, end_date.month - 1):
                n2 += 30
            n2 += self.count_leap_years(end_date)
            if end_date.day == 31:
                n = 0
            return (n2 - n1+n)
        else:
            return 0

    # def _check_finish_date(self):
    #     today = Pool().get('ir.date').today()
    #     if self.finished_date and self.finished_date > today:
    #         self.raise_user_error('finish_contract_out_date')


class StaffContractFuthermore(ModelSQL, ModelView):
    "Staff Contract Futhermore"
    __name__ = "staff.contract.futhermore"
    _rec_name = 'number'
    contract = fields.Many2One('staff.contract', 'Contract',
                               select=True, required=True)
    number = fields.Integer('Number', required=True, select=True)
    futhermore_date = fields.Date('Futhermore Date', required=True)
    kind = fields.Selection([
            ('salary', 'Salary'),
            ('end_contract', 'End Contract'),
            ('resignation', 'Resignation'),
            ('dismissal', 'Dismissal'),
            ('renewal', 'Renewal'),
        ], 'Kind', required=True)
    start_date = fields.Date('Start Contract', depends=['kind'],
                             states={
            'invisible': Eval('kind') != 'renewal',
        })
    end_date = fields.Date('End Contract', depends=['kind'],
                           states={
            'invisible': Eval('kind') == 'salary',
        })
    salary = fields.Numeric('New Salary', digits=(16, 2),
                            depends=['kind'], states={
            'invisible': Eval('kind') != 'salary',
        })
    salary_words = fields.Function(fields.Char('Salary (Words)'),
                                   'get_salary_to_words')
    transport_bonus = fields.Numeric('Transport Bonus', digits=(16, 2),
                                     depends=['kind'], states={
            'invisible': Eval('kind') != 'salary',
        })
    transport_bonus_words = fields.Function(fields.Char('Transport Bonus (Words)'),
                                            'get_salary_to_words')
    duration = fields.Function(fields.Integer('Duration', help="In days"),
                               'get_duration')
    full_salary = fields.Function(fields.Numeric('Full Salary'),
                                  'get_full_salary')
    full_salary_words = fields.Function(fields.Char('Transport Bonus (Words)'),
                                        'get_salary_to_words')
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ('finished', 'Finished'),
        ], 'State', required=True)
    description = fields.Char('Description', states={
        'readonly': Eval('state') != 'draft',
    }, depends=['state'])

    @staticmethod
    def default_state():
        return 'draft'

    def get_salary_to_words(self, name=None):
        if getattr(self, name[:-6]):
            return (conversor.cardinal(int(getattr(self, name[:-6])))).upper()
        else:
            return ''

    def get_duration(self, name=None):
        if self.kind in ('end_contract', 'renewal') and self.futhermore_date \
                and self.end_date:
            return self.contract.get_time_days(self.futhermore_date, self.end_date)

    def get_full_salary(self, name=None):
        res = Decimal('0')
        if self.salary:
            transport = Decimal('0')
            if self.transport_bonus:
                transport = self.transport_bonus
            res = self.salary + transport
        return res

    # @fields.depends('state', 'contract', 'end_date')
    # def on_change_state(self, name=None):
    #     if self.end_date and self.state != 'draft' and self.contract:
    #         contract.on_change_end_date()

    # @fields.depends('state', 'contract', 'end_date')
    # def on_change_end_date(self, name=None):
    #     print('ingresa a este registo')
    #     if self.end_date and self.state != 'draft' and self.contract:
    #         contract.on_change_end_date()


class ContractReturnActive(Wizard):
    'Contract Return Active'
    __name__ = 'staff.contract.return_active'
    start_state = 'return_active'
    return_active = StateTransition()

    def transition_return_active(self):
        cursor = Transaction().connection.cursor()
        ids = Transaction().context['active_ids']
        if ids:
            ids = str(ids[0])
            query = "UPDATE staff_contract SET state='active' WHERE id=%s"
            cursor.execute(query % ids)
        return 'end'


class ContractsEmployeesStart(ModelView):
    'Contracts Employees Start'
    __name__ = 'staff_co.print_contracts_employees.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    days_finish = fields.Integer('Days to Finish')
    include_inactives = fields.Boolean('Include Inactives')
    include_finished = fields.Boolean('Include Finished')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class ContractsEmployees(Wizard):
    'Contracts Employees'
    __name__ = 'staff_co.print_contracts_employees'
    start = StateView('staff_co.print_contracts_employees.start',
                      'staff_co.print_contracts_employees_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Print', 'print_', 'tryton-ok', default=True),
                          ])
    print_ = StateReport('staff_co.contracts_employees_report')

    def do_print_(self, action):
        Contract = Pool().get('staff.contract')
        dom_contracts = []
        if self.start.days_finish:
            today = date.today()
            end_date = today + timedelta(self.start.days_finish)
            dom_contracts = [
                ('finished_date', '<=', end_date),
                ('finished_date', '>=', today - timedelta(5)),
                ('finished_date', '!=', None)
            ]
        if not self.start.include_inactives:
            dom_contracts.append(
                ('employee.active', '=', True)
            )
        if not self.start.include_finished:
            dom_contracts.append(
                ('state', '!=', 'finished'))

        contracts = Contract.search(dom_contracts)

        data = {
            'ids': [contract.id for contract in contracts],
            'company': self.start.company.id,
            'days_finish': self.start.days_finish,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ContractsEmployeesReport(Report):
    'Contracts Employees Report'
    __name__ = 'staff_co.contracts_employees_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Company = Pool().get('company.company')
        report_context['days_finish'] = data['days_finish']
        report_context['company'] = Company(data['company'])
        return report_context


class AddFuthermoreStart(ModelView):
    'Add Futhermore Start'
    __name__ = 'staff_co.add_futhermore.start'
    futhermore_number = fields.Integer('Futhermore Number', required=True)
    kind = fields.Selection([
            ('salary', 'Salary'),
            ('end_contract', 'End Contract'),
            ('resignation', 'Resignation'),
            ('dismissal', 'Dismissal'),
            ('renewal', 'Renewal'),
        ], 'Kind', required=True)
    frecuency = fields.Integer('Frecuency', required=True,
                               help='In months')

    @staticmethod
    def default_frecuency():
        return 3


class AddFuthermore(Wizard):
    'Add Futhermore'
    __name__ = 'staff.add_futhermore'
    start = StateView('staff_co.add_futhermore.start',
                      'staff_co.add_futhermore_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Ok', 'accept', 'tryton-ok', default=True),
                          ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Futhermore = pool.get('staff.contract.futhermore')
        Contract = pool.get('staff.contract')
        futhermore_to_create = []
        id_ = Transaction().context['active_id']
        contract = Contract(id_)
        last_end_date = contract.end_date + timedelta(1)
        for i in range(self.start.futhermore_number):
            if i == 0:
                futhermore_date = contract.end_date + timedelta(1)
            else:
                futhermore_date = last_end_date + timedelta(1)
            end_date = self.add_months(
                    contract.end_date,
                    self.start.frecuency * (i + 1)
            )
            last_end_date = end_date
            futhermore_to_create.append({
                'number': (i + 1),
                'contract': id_,
                'kind': self.start.kind,
                'start_date': futhermore_date,
                'end_date': end_date,
                'futhermore_date': futhermore_date,
                'state': 'draft',
            })
        Futhermore.create(futhermore_to_create)
        return 'end'

    def add_months(self, date_, months_):
        return date_ + relativedelta(months=months_)


class ContractCertificationReport(Report):
    'Contract Certification'
    __name__ = 'staff_co.contract_certification_report'

    @classmethod
    def get_context(cls, records, header, data):
        Company = Pool().get('company.company')
        report_context = super().get_context(records, header, data)
        context = Transaction().context
        report_context['company'] = Company(context['company'])
        return report_context


class FinishContractReport(Report):
    'Finish Contract'
    __name__ = 'staff_co.finish_contract_report'

    @classmethod
    def get_context(cls, records, header, data):
        Company = Pool().get('company.company')
        report_context = super().get_context(records, header, data)
        context = Transaction().context
        report_context['company'] = Company(context['company'])
        return report_context

class PriorNotice(Wizard):
    'Prior Notice Wizard'
    __name__ = 'staff_co.prior_notice'
    start = StateView('staff_co.print_futhermore_contract.start',
                      'staff_co.futhermore_contract_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Ok', 'print_', 'tryton-print', default=True),
                          ])
    print_ = StateReport('staff_co.prior_notice_report')

    def do_print_(self, action):
        futhermore = self.start.futhermore
        if futhermore:
            futhermore = self.start.futhermore.id
        data = {
            'contract': self.start.contract.id,
            'futhermore': futhermore,
        }
        return action, data

    def transition_print_(self):
        return 'end'

class PriorNoticeReport(Report):
    'Prior Notice'
    __name__ = 'staff_co.prior_notice_report'

    @classmethod
    def get_context(cls, records, header, data):
        Company = Pool().get('company.company')
        Contract = Pool().get('staff.contract')
        Futhermore = Pool().get('staff.contract.futhermore')
        report_context = super().get_context(records, header, data)
        context = Transaction().context
        report_context['company'] = Company(context['company'])
        report_context['futhermore'] = Futhermore(data['futhermore'])
        report_context['contract'] = Contract(data['contract'])
        report_context['timedelta'] = timedelta
        return report_context


class LegalContractReport(Report):
    'Legal Contract Report'
    __name__ = 'staff_co.legal_contract_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class OperationalContractReport(Report):
    'Operational Contract Report'
    __name__ = 'staff_co.operational_contract_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class FuthermoreContractStart(ModelView):
    'Futhermore Contract Start'
    __name__ = 'staff_co.print_futhermore_contract.start'
    contract = fields.Many2One('staff.contract', 'Contract', required=True,
                               readonly=True)
    futhermore = fields.Many2One('staff.contract.futhermore',
                                 'Futhermore Number', domain=[
                                     ('contract', '=', Eval('contract'))
                                     ])

    @staticmethod
    def default_contract():
        return Transaction().context.get('active_id')


class FuthermoreContract(Wizard):
    'Print Futhermore Contract Wizard'
    __name__ = 'staff_co.print_futhermore_contract'
    start = StateView('staff_co.print_futhermore_contract.start',
                      'staff_co.futhermore_contract_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Ok', 'print_', 'tryton-print', default=True),
                          ])
    print_ = StateReport('staff_co.futhermore_contract_report')

    def do_print_(self, action):
        futhermore = self.start.futhermore
        if futhermore:
            futhermore = self.start.futhermore.id
        data = {
            'contract': self.start.contract.id,
            'futhermore': futhermore,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class FuthermoreContractReport(Report):
    'Futhermore Contract Report'
    __name__ = 'staff_co.futhermore_contract_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Contract = Pool().get('staff.contract')
        Futhermore = Pool().get('staff.contract.futhermore')
        user = Pool().get('res.user')(Transaction().user)
        report_context['records'] = [Futhermore(data['futhermore'])]
        report_context['contract'] = Contract(data['contract'])
        report_context['company'] = user.company
        return report_context


class ContractClausesReport(Report):
    'Contract Clauses'
    __name__ = 'staff_co.contract_clauses_report'

    @classmethod
    def get_context(cls, records, header, data):
        Company = Pool().get('company.company')
        report_context = super().get_context(records, header, data)
        context = Transaction().context
        report_context['company'] = Company(context['company'])
        return report_context


class ReturnEquipmentsReport(Report):
    'Return Equipments'
    __name__ = 'staff_co.return_equipment_report'

    @classmethod
    def get_context(cls, records, header, data):
        Company = Pool().get('company.company')
        report_context = super().get_context(records, header, data)
        context = Transaction().context
        report_context['company'] = Company(context['company'])
        return report_context
